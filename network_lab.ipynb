{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Network lab"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For this lab, we will be using fMRI connectivity data from http://umcd.humanconnectomeproject.org/, and analyzing the network of correlations between brain areas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np   # For matrix/array handling\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt   # For plotting\n",
    "import networkx as nx   # For network analysis\n",
    "from itertools import izip\n",
    "from sklearn import cluster\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Choosing a network"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# First, let's load in the correlations matrix.\n",
    "graph_loc = \"ADHD200_CC200_KKI_2371032_connectmat.txt\"\n",
    "with open(graph_loc) as fgraph:\n",
    "    for i,line in enumerate(fgraph):\n",
    "        row = np.array(map(float, line.strip().split()))\n",
    "        if i == 0:    # Set up the graph on our first pass through\n",
    "            N = len(row)\n",
    "            np_network = np.zeros((N,N), dtype=float)\n",
    "        np_network[i,:] = row\n",
    "\n",
    "print \"Created %d-user network, average correlation: %f\" %(N, np.mean(np_network))\n",
    "print \"Min correlation: %f\\tMax correlation: %f\" %(np.min(np_network), np.max(np_network))\n",
    "print \"Mean correlation: %f\\tMedian correlation: %f\" %(np.mean(np_network), np.median(np_network))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, correlations between areas varies a lot; however, most network analysis techniques become easier if we have a binary network. So we need to decide on a threshold--a correlation value above which we consider the correlation to be enough to draw an edge. There are tradeoffs in this decision--if we set the threshold very low, we will have an almost complete graph with no structure, but if we set the threshold very high, we will have an almost empty graph with no structure. Let's investigate how many edges we would have with various thresholds. Note that all thresholds ignore negative correlations, which did exist in the original data. We ignore them for now, but a more in-depth analysis would treat negative correlations and positive correlations as two different kinds of edges, resulting in a <b>heterogeneous network</b>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Try out various thresholds\n",
    "threshs = map(lambda x: x*.1, range(10))\n",
    "for thresh in threshs:\n",
    "    nonzeros = np.nonzero(np_network > thresh)[0]  # Threshold the network, and count the non-zero edges\n",
    "    print \"At threshold %f, we have %d of %d potential edges\" %(thresh, len(nonzeros)/2, (N**2 - N)/2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To observe how our results change as a result of different thresholds, we will work with two networks going forward: one thresholded at .4 and one thresholded at .7."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Now, build our graph in networkx so we can start analyzing it!\n",
    "thresh_net_4 = np_network > .4\n",
    "thresh_net_7 = np_network > .7\n",
    "\n",
    "G4 = nx.Graph()\n",
    "rows,cols = thresh_net_4.nonzero()\n",
    "for row,col in izip(rows,cols):  # Note this will add each edge twice, but networkx will just ignore redundant edges\n",
    "    G4.add_edge(row,col)\n",
    "print \"Created a networkx graph with %d nodes and %d edges\"  %(len(G4.nodes()), len(G4.edges()))  # Make sure it's the right size\n",
    "\n",
    "G7 = nx.Graph()\n",
    "rows,cols = thresh_net_7.nonzero()\n",
    "for row,col in izip(rows,cols):\n",
    "    G7.add_edge(row,col)\n",
    "print \"Created a networkx graph with %d nodes and %d edges\"  %(len(G7.nodes()), len(G7.edges()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Degree distribution and centrality\n",
    "Now that we have our network, we can start analyzing its properties. The first thing you may want to do is visualize the network, but unfortunately, this doesn't tell you a whole lot on its own. Note that the network visualization algorithm is stochastic--if you want, you can run this block several times and get slightly different visualizations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Visualize the networks\n",
    "fig, axs = plt.subplots(ncols=2)  # Make side-by-side plots, like MATLAB's subplot function\n",
    "fig.subplots_adjust(right=1.8, wspace=.5, top=1.4)  # This allows us to make the subplots bigger and more spaced out\n",
    "nx.draw(G4,ax=axs[0])\n",
    "axs[0].set_title(\".4 network visualization\")\n",
    "nx.draw(G7,ax=axs[1])\n",
    "axs[1].set_title(\".7 network visualization\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tells us some things:  in the .7 network, most nodes just link to a few other nodes, but not in a way that forms coherent clusters. We see stronger interconnections in the .4 network. However, in both visualizations, most of the nodes just seem to be on top of each other. So we will need to find other ways to understand what's going on, some of which will aid us in visualization. One of the first things to look at is the degree distribution--how many nodes does each node connect to?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Get degree distribution\n",
    "degree_dict4 = nx.degree(G4)  # Returns a dictionary from node name to node degree\n",
    "degree_arr4 = np.array(degree_dict4.values())  # Put this into a numpy array for easier analysis\n",
    "print \"Min/mean/median/max degree (.4 network):\\t%d\\t%f\\t%f\\t%d\" %(np.min(degree_arr4), np.mean(degree_arr4),\n",
    "                                                     np.median(degree_arr4), np.max(degree_arr4))\n",
    "degree_dict7 = nx.degree(G7)  # Returns a dictionary from node name to node degree\n",
    "degree_arr7 = np.array(degree_dict7.values())  # Put this into a numpy array for easier analysis\n",
    "print \"Min/mean/median/max degree (.7 network):\\t%d\\t%f\\t%f\\t%d\" %(np.min(degree_arr7), np.mean(degree_arr7),\n",
    "                                                     np.median(degree_arr7), np.max(degree_arr7))\n",
    "# And let's make a histogram\n",
    "fig, axs = plt.subplots(ncols=2)\n",
    "fig.subplots_adjust(wspace=.5, right=1.5)\n",
    "axs[0].hist(degree_arr4, bins=15)\n",
    "axs[0].set_title(\"Degree distribution (.4 network)\")\n",
    "axs[1].hist(degree_arr7, bins=15)\n",
    "axs[1].set_title(\"Degree distribution (.7 network)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can glean a few things from this analysis. As we would expect, the .4-thresholded network has nodes with many more connections than the .7 network. Interestingly, we see that the degree distribution in the .4 network isn't particularly skewed--there are some more nodes in the low-degree part of the graph than the high-degree part, but it isn't that clear. However, the .7 network shows a clear power law degree distribution: we have many nodes with only 1 or 2 edges, and the amount of nodes with higher numbers of edges decays exponentially. This power law pattern is very common in many network settings--anywhere where you have a few popular nodes and many not-as-popular nodes will result in this pattern."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This transitions us nicely into the concept of <b>network centrality</b>. In any network, we may want to know who the most important nodes are, but how do we define \"important\"? We've already seen one way--by the degree of an edge. However, there are other ways that we will investigate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# First, we look at degree centrality, which says that a node is as important as its connections\n",
    "np.random.seed(11)  # Hold the random seed fixed so we always generate the same graph\n",
    "fig,axs = plt.subplots(ncols=2)\n",
    "fig.subplots_adjust(right=1.7, wspace=.5, top=1.1)\n",
    "degree_centrality_4 = nx.degree_centrality(G4)\n",
    "nx.draw_networkx(G4,with_labels=False, nodelist=degree_centrality_4.keys(), node_color=degree_centrality_4.values(),\n",
    "                 cmap=plt.get_cmap('Blues'), ax=axs[0])   # Color the nodes by centrality value\n",
    "axs[0].set_title(\"Degree centrality (.4)\")\n",
    "degree_centrality_7 = nx.degree_centrality(G7)\n",
    "nx.draw_networkx(G7,with_labels=False, nodelist=degree_centrality_7.keys(), node_color=degree_centrality_7.values(),\n",
    "                 cmap=plt.get_cmap('Blues'), ax=axs[1])   # Color the nodes by centrality value\n",
    "axs[1].set_title(\"Degree centrality (.7)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, degree centrality misses a lot: am I really more central to the network if I'm connected to 50 unimportant people than if I'm connected to 2 very important ones? We ahve three other standard measures of centrality that attempt to overcome this defecit:\n",
    "1. Betweenness centrality: This looks at how important a \"bridge\" a node is between other nodes. It is computed by looking at the number of shortest paths between edges in the network pass through this one, so it will be higher if many nodes need to pass through this node on their shortest paths to other nodes.\n",
    "2. Closeness centrality: This says that a node is more central if it has lower \"degrees of separation\" from most nodes in the network. It is computed as 1 over the average distance on the grahp from this node to all the others in the network.\n",
    "3. Eigenvector centrality: This is essentially equivalent to the PageRank algorithm that Google uses to rank the importance of websites. It says that a node is important if it is connected to other important nodes in the network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.random.seed(11)\n",
    "fig,axs = plt.subplots(ncols=2)\n",
    "fig.subplots_adjust(right=1.7, wspace=.5, top=1.1)\n",
    "betweenness_centrality_4 = nx.betweenness_centrality(G4)\n",
    "nx.draw_networkx(G4,with_labels=False, nodelist=betweenness_centrality_4.keys(), node_color=betweenness_centrality_4.values(),\n",
    "                 cmap=plt.get_cmap('Blues'), ax=axs[0])   # Color the nodes by centrality value\n",
    "axs[0].set_title(\"Betweenness centrality (.4)\")\n",
    "betweenness_centrality_7 = nx.betweenness_centrality(G7)\n",
    "nx.draw_networkx(G7,with_labels=False, nodelist=betweenness_centrality_7.keys(), node_color=betweenness_centrality_7.values(),\n",
    "                 cmap=plt.get_cmap('Blues'), ax=axs[1])   # Color the nodes by centrality value\n",
    "axs[1].set_title(\"Betweenness centrality (.7)\")\n",
    "plt.show()\n",
    "\n",
    "np.random.seed(11)\n",
    "fig,axs = plt.subplots(ncols=2)\n",
    "fig.subplots_adjust(right=1.7, wspace=.5, top=1.1)\n",
    "closeness_centrality_4 = nx.closeness_centrality(G4)\n",
    "nx.draw_networkx(G4,with_labels=False, nodelist=closeness_centrality_4.keys(), node_color=closeness_centrality_4.values(),\n",
    "                 cmap=plt.get_cmap('Blues'), ax=axs[0])   # Color the nodes by centrality value\n",
    "axs[0].set_title(\"Closeness centrality (.4)\")\n",
    "closeness_centrality_7 = nx.closeness_centrality(G7)\n",
    "nx.draw_networkx(G7,with_labels=False, nodelist=closeness_centrality_7.keys(), node_color=closeness_centrality_7.values(),\n",
    "                 cmap=plt.get_cmap('Blues'), ax=axs[1])   # Color the nodes by centrality value\n",
    "axs[1].set_title(\"Closeness centrality (.7)\")\n",
    "plt.show()\n",
    "\n",
    "\n",
    "np.random.seed(11)\n",
    "fig,axs = plt.subplots(ncols=2)\n",
    "fig.subplots_adjust(right=1.7, wspace=.5, top=1.1)\n",
    "eigenvector_centrality_4 = nx.eigenvector_centrality(G4,max_iter=200)  # specify max_iter or else it doesn't converge\n",
    "nx.draw_networkx(G4,with_labels=False, nodelist=eigenvector_centrality_4.keys(), node_color=eigenvector_centrality_4.values(),\n",
    "                 cmap=plt.get_cmap('Blues'), ax=axs[0])   # Color the nodes by centrality value\n",
    "axs[0].set_title(\"Eigenvector centrality (.4)\")\n",
    "eigenvector_centrality_7 = nx.eigenvector_centrality(G7)\n",
    "nx.draw_networkx(G7,with_labels=False, nodelist=eigenvector_centrality_7.keys(), node_color=eigenvector_centrality_7.values(),\n",
    "                 cmap=plt.get_cmap('Blues'), ax=axs[1])   # Color the nodes by centrality value\n",
    "axs[1].set_title(\"Betweenness centrality (.7)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some differences between the types of centrality are clear from the diagrams, but not a whole lot. One thing we can do to make the differences clearer is plot the different kinds of centrality against each other"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "degree_vec_4 = np.array([value for key,value in sorted(degree_centrality_4.items())])\n",
    "closeness_vec_4 = np.array([value for key,value in sorted(closeness_centrality_4.items())])\n",
    "betweenness_vec_4 = np.array([value for key,value in sorted(betweenness_centrality_4.items())])\n",
    "eigenvector_vec_4 = np.array([value for key,value in sorted(eigenvector_centrality_4.items())])\n",
    "\n",
    "# Now, print all 6 pairwise plots\n",
    "plt.subplot(321)\n",
    "plt.subplots_adjust(wspace=.5,hspace=.5, right=1.5, top=2)\n",
    "plt.plot(degree_vec_4,closeness_vec_4,'b+')\n",
    "plt.xlabel(\"degree\")\n",
    "plt.ylabel(\"closeness\")\n",
    "\n",
    "plt.subplot(322)\n",
    "plt.plot(degree_vec_4,betweenness_vec_4,'b+')\n",
    "plt.xlabel(\"degree\")\n",
    "plt.ylabel(\"betweenness\")\n",
    "\n",
    "plt.subplot(323)\n",
    "plt.plot(degree_vec_4,eigenvector_vec_4,'b+')\n",
    "plt.xlabel(\"degree\")\n",
    "plt.ylabel(\"eigenvector\")\n",
    "\n",
    "plt.subplot(324)\n",
    "plt.plot(closeness_vec_4,betweenness_vec_4,'b+')\n",
    "plt.xlabel(\"closeness\")\n",
    "plt.ylabel(\"betweenness\")\n",
    "\n",
    "plt.subplot(325)\n",
    "plt.plot(closeness_vec_4,eigenvector_vec_4,'b+')\n",
    "plt.xlabel(\"closeness\")\n",
    "plt.ylabel(\"eigenvector\")\n",
    "\n",
    "plt.subplot(326)\n",
    "plt.plot(betweenness_vec_4,eigenvector_vec_4,'b+')\n",
    "plt.xlabel(\"betweenness\")\n",
    "plt.ylabel(\"eigenvector\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, the types of centrality tend to be fairly correlated with each other. Closeness, degree, and betweenness especially all seem to say similar things; however, eigenvector sometimes shows a different story. For example, on the eigenvector vs degree plot, we see two sets of nodes: one set where the degree and eigenvector centralities correlate very well, and another set with high degree centrality with low eigenvector centrality. The latter nodes are those that have a lot of neighbors, but whose neighbors are unimportant to the rest of the graph.\n",
    "\n",
    "We can look at the same plots for the .7 network to see if there's any big differences"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "degree_vec_7 = np.array([value for key,value in sorted(degree_centrality_7.items())])\n",
    "closeness_vec_7 = np.array([value for key,value in sorted(closeness_centrality_7.items())])\n",
    "betweenness_vec_7 = np.array([value for key,value in sorted(betweenness_centrality_7.items())])\n",
    "eigenvector_vec_7 = np.array([value for key,value in sorted(eigenvector_centrality_7.items())])\n",
    "\n",
    "# Now, print all 6 pairwise plots\n",
    "plt.subplot(321)\n",
    "plt.subplots_adjust(wspace=.5,hspace=.5, right=1.5, top=2)\n",
    "plt.plot(degree_vec_7,closeness_vec_7,'r+')\n",
    "plt.xlabel(\"degree\")\n",
    "plt.ylabel(\"closeness\")\n",
    "\n",
    "plt.subplot(322)\n",
    "plt.plot(degree_vec_7,betweenness_vec_7,'r+')\n",
    "plt.xlabel(\"degree\")\n",
    "plt.ylabel(\"betweenness\")\n",
    "\n",
    "plt.subplot(323)\n",
    "plt.plot(degree_vec_7,eigenvector_vec_7,'r+')\n",
    "plt.xlabel(\"degree\")\n",
    "plt.ylabel(\"eigenvector\")\n",
    "\n",
    "plt.subplot(324)\n",
    "plt.plot(closeness_vec_7,betweenness_vec_7,'r+')\n",
    "plt.xlabel(\"closeness\")\n",
    "plt.ylabel(\"betweenness\")\n",
    "\n",
    "plt.subplot(325)\n",
    "plt.plot(closeness_vec_7,eigenvector_vec_7,'r+')\n",
    "plt.xlabel(\"closeness\")\n",
    "plt.ylabel(\"eigenvector\")\n",
    "\n",
    "plt.subplot(326)\n",
    "plt.plot(betweenness_vec_7,eigenvector_vec_7,'r+')\n",
    "plt.xlabel(\"betweenness\")\n",
    "plt.ylabel(\"eigenvector\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These graphs have more obvious artifacts--since there are fewer possible connections, there are fewer possible degree centrality values. Here, we see degree centrality matching up much more nicely with eigenvector centrality--we might hypothesize that this is the case because the connections the .7 network dropped were ones with \"unimportant\" nodes, so there is less of an opportunity for a node to be connected to lots of these, giving high degree and low eigenvector centrality."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise\n",
    "We can look at how centrality differs across the networks--which one would we expect has more central nodes? To do this, we will do the following:\n",
    "1. Choose one of the centrality measures and find its mean value for both networks\n",
    "2. Plot the two centralities in different colors, and plot each mean in a different color.\n",
    "3. Change two lines in your code to make it work for a different centrality. How do the results for this one look compared to the other?\n",
    "NOTE: in all these exercises, anything you have to do will be put in triple \"\"\"quotes\"\"\", everything else should work as is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Choose centrality measure--you can just set this equal to one of our precomputed centrality vectors\n",
    "my_centrality_4 = \"\"\"something\"\"\"\n",
    "my_centrality_7 = \"\"\"something\"\"\"\n",
    "\n",
    "# Compute means\n",
    "mean_4 = np.mean(\"\"\"something\"\"\")\n",
    "mean_7 = np.mean(\"\"\"something\"\"\")\n",
    "\n",
    "# Make x vectors so we can plot each measure vertically but not on top of each other, and prepare the mean vector\n",
    "x_4 = np.ones(len(my_centrality_4)) + 3\n",
    "x_7 = np.ones(len(my_centrality_7)) + 6\n",
    "mean_y = np.array([mean_4,mean_7])\n",
    "mean_x = np.array([4,7])\n",
    "\n",
    "# Plot it all on one axis\n",
    "\n",
    "plt.plot(\"\"\"x axis 4\"\"\", \"\"\"y axis 4\"\"\", 'r+', \"\"\"x axis 7\"\"\", \"\"\"y axis 7\"\"\", 'b+', mean_x, mean_y, 'ks')\n",
    "plt.xlim([3.5,7.5])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Miscellaneous network statistics\n",
    "Networks are very complicated objects, and we can't gain an appreciation for what's going on through a single number. However, there are some statistics that give you an idea about certain properties of a network that you may find useful. Here, we explore a couple, and look at how they differ between the .4 and .7 networks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we have network assortativity: the tendency of nodes to connect to nodes of similar degree. We might expect this measure to be lower in \"core-perihery\" networks: networks where some people act as information hubs and the rest act as receivers of information. We expect it to be low in this case because high-degree hubs associate with low-degree receivers rather than with other hubs. However, in friendship networks, we might expect this number to be higher."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print \"Assortativity\\t.4 network: %f\\t.7 network: %f\" %(nx.degree_assortativity_coefficient(G4),nx.degree_assortativity_coefficient(G7))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we see that the .7 network is slightly more assortative. This leads credence to our hypothesis before that the .7 network has fewer \"unimportant\" nodes connecting to high-degree nodes, giving us that interesting pattern in the .4 network's degree vs. eigenvector centrality plot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we look at transitivity. This is the tendency of network connections to obey the mathematical relationship of transitivity: if A<->B and B<->C, how often does A<->C? It is computed by dividing the total number of complete triangles (i.e., 3-cliques) by the total number of possible triangles (defined as any set of three nodes where A<->B and B<->C)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print \"Transitivity\\t.4 network: %f\\t.7 network: %f\" %(nx.transitivity(G4), nx.transitivity(G7))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We might be interested in how clustered a network is--do nodes tend to connect in compact groups, or randomly associate with nodes at random in the network. We can compute this at a node level using the clustering coefficient, which looks at how many triangles a node is involved in relative to the total number of triangles a node with that degree could be involved in. To get an idea of how clustered the network is overall, we can look at the average clustering coefficient."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print \"Average clustering coefficient\\t.4 network: %f\\t.7 network: %f\" %(nx.average_clustering(G4), nx.average_clustering(G7))\n",
    "\n",
    "# Let's also plot a histogram of the clustering coefficients for each graph\n",
    "clust_coefs_4 = np.array(nx.clustering(G4).values())  # nx.clustering() returns a dict, so we just take its values\n",
    "clust_coefs_7 = np.array(nx.clustering(G7).values())\n",
    "fig, axs = plt.subplots(ncols=2)\n",
    "fig.subplots_adjust(wspace=.5, right=1.5)\n",
    "axs[0].hist(clust_coefs_4, bins=20)\n",
    "axs[0].set_title(\"Clustering coefficients distribution (.4 network)\")\n",
    "axs[1].hist(clust_coefs_7, bins=20)\n",
    "axs[1].set_title(\"Clustering coefficients distribution (.7 network)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It shouldn't surprise us that the .4 network is more clustered than the .7 network--since it's so much more tense, there is a much greater opportunity for clusters to emerge."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we might want to know the degrees of separation for this network: what's the longest path you'd have to take to navigate between the two furthest-apart nodes? \n",
    "NOTE: We only do this for the .4 network--since the .7 network isn't fully connected, it has an infinite diameter and throws an error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print \"Diameter\\t.4 network: \" + str(nx.diameter(G4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise\n",
    "The network diameter is the longest shortest path in the whole network, but there is an analogous node-level statistic called eccentricity--the shortest path you can take to get from this node to the node furthest away. \n",
    "1. First, plot a histogram of the eccentricities of the network. This histogram looks a little different from the others we've seen--what does it tell us about the network? Note: the function you want to use is nx.eccentricities(), but this will return a dict, not a vector, so you will have to convert it in the same way I did above for centralities.\n",
    "2. We might expect that a node's eccentricity might be related to its closeness centrality, which is larger the closer you are to other nodes on average. How would you expect the two to be related? Make a scatterplot of one against the other to see if you can see a trend."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Calculate eccentricities\n",
    "eccentricities_dict_4 = \"\"\"something\"\"\"\n",
    "# Turn this dictionary into an array for plotting\n",
    "eccentricities_vec_4 = \"\"\"something to convert a dict into a vec while preserving indices\"\"\"\n",
    "# Plot a histogram of the eccentricities\n",
    "\"\"\"plot a histogram here\"\"\"\n",
    "plt.xlabel(\"Eccentricity\")\n",
    "plt.ylabel(\"Frequency\")\n",
    "plt.show()\n",
    "# Plot eccentricities against closeness centrality\n",
    "\"\"\"scatter plot of eccentricities against closeness centrality here\"\"\"\n",
    "plt.xlabel(\"Eccentricity\")\n",
    "plt.ylabel(\"Closeness centrality\")\n",
    "plt.xlim([3.8,5.2])  # This line makes sure youc an see everything in the plot clearly\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Clustering\n",
    "Can our nodes be split into groups that tend to interact within the group and not without? What do these clusters look like? For this section, we will go back to the original correlation matrix: network clustering algorithms take a matrix of differences between nodes as input and reports a clustering as output. Typically, clustering algorithms require the user to choose a number of clusters they would like to split their data into.\n",
    "For this exercise, we will use an algorithm called <b>spectral clustering</b>, which takes a similarity matrix (like our correlation matrix), embeds the nodes into a low-dimensional space, and clusters them in that space. Try using different numbers of clusters  and see "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# First, let's remove negatives from our correlation matrix and set them equal to 0.\n",
    "thresh_net = np_network.copy()\n",
    "thresh_net[thresh_net < 0] = 0\n",
    "\n",
    "n_clusters = 4\n",
    "spectral = cluster.SpectralClustering(n_clusters=n_clusters, affinity='precomputed')\n",
    "clusts = spectral.fit_predict(thresh_net)\n",
    "np.random.seed(11)\n",
    "\n",
    "print \"Number of nodes in each cluster\"\n",
    "for clust in xrange(n_clusters):\n",
    "    print \"Cluster %d: %d\" %(clust,np.sum(clusts == clust))\n",
    "    \n",
    "clust_colors = clusts / float(N)\n",
    "nx.draw_networkx(G4,with_labels=False, node_color=clust_colors,\n",
    "                 cmap=plt.get_cmap('gist_ncar'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also look at agglomerative clustering, which starts with every node in its own cluster, and greedily merges clusters together until we have the desired amount. A very important parameter for this algorithm is the linkage, which tells us how to combine clusters. Do we want to combine two clusters together if on average, the nodes in one cluster are close to those in the other (\"average\"), if the node in cluster 1 that is furthest away from any node in cluster 2 is still pretty close (\"complete\"), or if the resultant cluster will be low-variance (\"ward\")?\n",
    "\n",
    "Note that this algorithm, unlike the last, doesn't take a similarity matrix: it takes a feature matrix (where each row is a node and each column is a \"feature\"). We can still use our similarity matrix for this, but we will want to amend our correlation matrix to make each node close to itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# First, put ones along the diagonal of the thresh_net, to indicate that each node is perfectly correlated with itself\n",
    "thresh_net_with_diag = thresh_net + np.diag(np.ones(N)) \n",
    "\n",
    "n_clusters = 4\n",
    "linkage = \"average\"\n",
    "agg = cluster.AgglomerativeClustering(n_clusters=n_clusters, linkage=linkage)\n",
    "agg_clusts = agg.fit_predict(thresh_net_with_diag)\n",
    "\n",
    "print \"Number of nodes in each cluster\"\n",
    "for clust in xrange(n_clusters):\n",
    "    print \"Cluster %d: %d\" %(clust,np.sum(agg_clusts == clust))\n",
    "\n",
    "np.random.seed(11)\n",
    "clust_colors = agg_clusts / float(N)\n",
    "nx.draw_networkx(G4,with_labels=False, node_color=clust_colors,\n",
    "                 cmap=plt.get_cmap('gist_ncar'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise\n",
    "We expect that a cluster might have some different properties from the graph as a whole. In this exercise, we'll investigate some of the properties of the sub-network of a single cluster and compare it to the network as a whole."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Create a boolean array where only node indices in our cluster are 1, and everything else is 0.\n",
    "subnetwork_nodes = \"\"\"cluster vec\"\"\" == \"\"\"cluster value\"\"\"\n",
    "subnetwork_graph = thresh_net_4[np.ix_(subnetwork_nodes, subnetwork_nodes)]  # Magic to index graph properly\n",
    "\n",
    "# Create new graph\n",
    "G_clust = \"\"\"create the graph\"\"\"\n",
    "rows,cols = subnetwork_graph.nonzero()\n",
    "for row,col in izip(rows,cols):  # Note this will add each edge twice, but networkx will just ignore redundant edges\n",
    "    \"\"\"add this edge to G_clust\"\"\"\n",
    "    \n",
    "# Degree\n",
    "degree_clust = \"\"\"Compute degree\"\"\"\n",
    "degree_arr_clust = np.array(degree_clust.values())  # Turn degree \n",
    "\n",
    "fig, axs = plt.subplots(ncols=2)\n",
    "fig.subplots_adjust(wspace=.5, right=1.5)\n",
    "axs[0].hist(degree_arr4, bins=20)\n",
    "axs[1].\"\"\"histogram of the cluster degree, following from the line above\"\"\"\n",
    "\n",
    "# Now, clustering\n",
    "clustering_G4 = \"\"\"average clustering coefficient for G4\"\"\"\n",
    "clustering_G_clust = \"\"\"average clustering coefficient for G_clust\"\"\"\n",
    "print \"Average clustering coefficient\\t.4 network: %f\\tCluster: %f\" %(clustering_G4, clustering_G_clust)\n",
    "\n",
    "# Now, diameter\n",
    "diameter_G4 = \"\"\"diameter\"\"\"\n",
    "diameter_G_clust = \"\"\"diameter\"\"\"\n",
    "print \"Diameter\\t.4 network: %f\\tCluster: %f\" %(diameter_G4, diameter_G_clust)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [Root]",
   "language": "python",
   "name": "Python [Root]"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
