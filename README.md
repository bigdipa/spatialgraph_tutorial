
Students will experiment with computing spatial statistics of structures
extracted from image data and perform graph-based analysis of connectivity
data.

Hands-on exercise: 

Students will experiment with density estimation and regression techniques to
model distributions of cells and gene expression patterns measured in tissue.
Students will also fit a network model to functional correlations observed
between different brain regions in human fMRI data and analyze the properties
of the resulting graph. Alternately, students will construct a connectivity
graph derived from projections of traced neurons between different anatomically
defined regions in the fly brain. They will analyze the in terms of local
degree distributions and path lengths, and apply a latent clustering algorithm
to find blocks of strongly connected sub-networks. 

Key Concepts Learned:

Students will gain hands-on experience using standard Python libraries to load
and process spatial distribution and connectivity data derived from large image
datasets and perform rigorous statistical fitting and analysis of this data.

References:

http://www.flycircuit.tw/

http://umcd.humanconnectomeproject.org/


